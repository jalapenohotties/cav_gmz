#include "IntraFrame.hpp"

IntraFrame::IntraFrame(string filepath) : filepath(filepath) {
    cout << "IntraFrame::IntraFrame() initialized. input:" << filepath << endl;
    frameCount = 0;
}

void IntraFrame::encode() {
    /***
     * Intra frame encoding flow is the following:
     * 1. Creates an Video() from the input
     * 2. Uses the Predictor() frame by frame
     * 3. Writes frame by frame
     */

    Video video = Video(filepath);

    string outFile = filepath + FILE_EXTENSION;
    Utils utils = Utils(outFile, "w");

    double t;

    utils.write_value(video.getCols());
    utils.write_value(video.getRows());
    utils.write_value(video.getFPS());
    utils.write_value(video.nrFrames());

    while (video.hasNext()) {

        frameCount++;
        t = (double) getTickCount();

        Mat *frame = video.getNext();
        calc_Mat_residuals(frame);
        utils.mat_to_bs(frame);

        t = 1000 * ((double) getTickCount() - t) / getTickFrequency();
        cout << "\r" << "IntraFrame::encode(): frame " << frameCount << " in " << t << "ms" << flush;
    }

    utils.close();

}

void IntraFrame::calc_Mat_original(Mat *mat) {
    Predict p = Predict();
    p.calcOriginals(mat);
}


void IntraFrame::calc_Mat_residuals(Mat *mat) {
    Predict p = Predict();
    p.calcResiduals(mat);
}

void IntraFrame::decode() {

    string inFile = filepath;
    Utils utils = Utils(inFile, "r");

    unsigned int nCols = utils.read_value();
    unsigned int nRows = utils.read_value();
    unsigned int fps = utils.read_value();
    unsigned int frames = utils.read_value();

    Video video = Video(nCols, nRows, fps);

    double t;

    for (unsigned int i = 0; i < frames; i++) {
        t = (double) getTickCount();

        Mat *tmp = new Mat();
        tmp->create(nCols, nRows, CV_8UC3);

        utils.bs_to_mat(tmp);
        calc_Mat_original(tmp);

        //imshow("Display Image", *tmp);
        //waitKey(0);

        video.addFrame(tmp);

        t = 1000 * ((double) getTickCount() - t) / getTickFrequency();
        cout << "\r" << "IntraFrame::decode(): frame " << i + 1 << "/" << frames << " in " << t << "ms" << flush;
    }
    cout << endl;


    // play the video
    int i = 0;
    while (video.hasNext()) {
        cout << "\r" << "IntraFrame::decode(): playing frame " << i++ + 1 << "/" << frames << "..." << flush;
        Mat *tmp = video.getNext();
        imshow("Display Image", *tmp);
        if (waitKey((int) (1.0 / fps * 1000)) >= 0) break;
    }


}
