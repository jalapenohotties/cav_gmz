#ifndef PROJECT3_VIDEO_H
#define PROJECT3_VIDEO_H

#include <opencv2/opencv.hpp>
#include "vector"
#include "string"
#include <fstream>

class Video {

public:
    /**
     * Initializes an Video container from file
     * @param filepath to the video
     */
    Video(std::string filepath);

    /**
     * Initializes an empty video container
     * @param nCols number of cols
     * @param nRows number of rows
     * @param fps speed
     */
    Video(unsigned int nCols, unsigned int nRows, unsigned int fps);

    /**
     * Checks if there's video frames to retrieve from the file
     * @return true if has next. False if not
     */
    bool hasNext(); // has more frames or not

    /**
     * Gets next video frame pointer
     * @return
     */
    cv::Mat *getNext();

    /**
     * Returns video FPS
     * @return fps
     */
    unsigned int getFPS();

    /**
     * Returns row number (sabe as Mat rows)
     * @return rows
     */
    unsigned int getRows();

    /**
     * Returns Cols number (same as Mat cols)
     * @return cols
     */
    unsigned int getCols();

    /**
     * Returns the number of frames in the video
     * @return number of frames
     */
    unsigned int nrFrames();

    /**
     * Adds an frame to the video array
     * @param mat pointer to add
     */
    void addFrame(cv::Mat *mat);

private:
    unsigned int nCols, nRows, type, fps;

    //Vector containing all frames of the video
    std::vector<cv::Mat *> arrFrames;

    // controls the corrent getNext()
    unsigned int pos;
};


#endif //PROJECT3_VIDEO_H
