#include "Predict.hpp"

 Vec3b Predict::predict_Vec3b(Vec3b aa, Vec3b bb, Vec3b cc) {
    /**
     * +---+
     * |c|b|    x =  | min(a, b) if c >= max(a, b)
     * +---+         | max(a, b) if c <= min(a, b)
     * |a|X|         | a + b - c otherwise
     * +---+
     */

    uchar a, b, c;
    Vec3b tmp;

    for (int i = 0; i < 3; i++) {
        a = aa[i];
        b = bb[i];
        c = cc[i];

        if (c >= max(a, b))
            tmp[i] = min(a, b);
        else if (c <= min(a, b))
            tmp[i] = max(a, b);
        else
            tmp[i] = (a + b - c);
    }

    return tmp;

}


Mat Predict::calcOriginals(Mat *original) {

    Vec3b pred;

    Mat res = original->clone();

    for (int i = 1; i < res.rows; i++) {
        for (int j = 1; j < res.cols; j++) {

            pred = predict_Vec3b(original->at<Vec3b>(i, j - 1),
                                 original->at<Vec3b>(i - 1, j),
                                 original->at<Vec3b>(i - 1, j - 1));


            // restore the predictor error
            original->at<Vec3b>(i, j) = undo_calcVec3bDiff(pred, original->at<Vec3b>(i, j));

        }
    }

    return res;
}

Mat Predict::calcResiduals(Mat *mat) {

    Mat original = mat->clone(); // mat with the final residuals
    Mat matPred = mat->clone(); // mat with the predicted results
    Vec3b pred;

    //cv::imshow(string("entrada").c_str(), mat);
    //waitKey(0);

    for (int i = 1; i < original.rows; i++) {
        for (int j = 1; j < original.cols; j++) {

            pred = predict_Vec3b(original.at<Vec3b>(i, j - 1),
                                 original.at<Vec3b>(i - 1, j),
                                 original.at<Vec3b>(i - 1, j - 1));


            matPred.at<Vec3b>(i, j) = pred;

            mat->at<Vec3b>(i, j) = calcVec3bDiff(original.at<Vec3b>(i, j), pred);

        }
    }

    //cv::imshow(string("predicted").c_str(), matPred);
    //cv::imshow(string("residuais").c_str(), residuals);
    //waitKey(0);

    return matPred;

}

Vec3b Predict::undo_calcVec3bDiff(Vec3b a, Vec3b residual) {

    Vec3b result;
    int tmp;

    for (int i = 0; i < 3; i++) {
        tmp = (residual[i] % 2 == 0) ? residual[i] / 2 : (1 + residual[i]) / -2;
        result[i] = (uchar) (a[i] + tmp);
    }

    return result;

}

Vec3b Predict::calcVec3bDiff(Vec3b a, Vec3b b) {

    Vec3b result;
    int res;

    for (int i = 0; i < 3; i++) {
        res = a[i] - b[i];
        result[i] = res < 0 ? (-2 * res) - 1 : res * 2;
    }

    return result;
}