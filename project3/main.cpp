#include <iostream>
#include <getopt.h>
#include "IntraFrame.hpp"
#include "InterFrame.hpp"
#include "MotionCompensation.hpp"

using namespace std;

void printMenu() {
    cout << "CAV Video encoder" << endl;
    cout << endl;
    cout << "-b <X>x<Y> Define block size (default 8x8)" << endl;
    cout << "-1         Use simple intra frame encoding (phase 1)" << endl;
    cout << "-2         Use iter frame encoding (phase 2)" << endl;
    cout << "-3         Use block matching for encoding (phase 3)" << endl;
    cout << "-d file    Decode an encoded CAV file" << endl;
    cout << "-h         Print this help" << endl;
    cout << "" << endl;
    cout << "Versions are cumulative (-3 includes -2 and -1)" << endl;
    cout << "" << endl;
    cout << "Examples" << endl;
    cout << "1) ./cav -1 video.in" << endl;
    cout << "Produces and encoded file using version 1 of the encoder" << endl;
    cout << "" << endl;
    cout << "2) ./cav -d -1 video.cav" << endl;
    cout << "Decodes video.cav.v1 version 1 back to original format" << endl;
    cout << "" << endl;
    cout << "3) ./cav -2 -b 12x10 video.in" << endl;
    cout << "Encodes using version 1 of the encoder with an 12x10 block size " << endl;
    cout << "" << endl;

}

int main(int argc, char **argv) {

    if (argc == 1)
        cout << "Usage: -h for help" << endl;

    // Read options from arguments
    extern int opterr; // suppress getopt error message
    opterr = 0;

    // codec version, custom_block, decode flags
    bool v1 = false, v2 = false, v3 = false, custom_block = false, decode = false;
    // block size
    int x = 0, y = 0;
    int index;
    string filepath;

    char c;
    while ((c = getopt(argc, argv, "h123db:p:")) != -1)
        switch (c) {
            case 'h':
                printMenu();
                return 0;
            case '1':
                v1 = true;
                break;
            case '2':
                v2 = true;
                break;
            case '3':
                v3 = true;
                break;
            case 'd':
                decode = true;
                break;
            case 'b': {
                string delimiter = "x";
                string s(optarg);
                size_t pos = 0;

                int *block[] = {&x, &y};

                for (int i = 0; i < 2; i++) {
                    pos = s.find(delimiter);

                    string number(s.substr(0, pos));
                    *block[i] = stoi(number);
                    s.erase(0, pos + delimiter.length());
                }

                custom_block = true;
                break;
            }
            case 'p': {
                string optarg;

            }
            default:
                cout << "Invalid arguments" << endl;
                cout << "Run with -h for help" << endl;
                return 0;
        }

    for (index = optind; index < argc; index++) {
        filepath = string(argv[index]);
        cout << "Using file in: " << filepath << endl;
    }

    if (v3) {
        cout << "Encoding using version 3";

        if (custom_block) {
            cout << "... with custom block of " << x << "x" << y << endl;
            // chamada ao encoder com custom block
        } else {
            cout << endl;
        }

    } else if (v2) {
        EncodingInterface *ei;
        if (custom_block) {
            cout << "... with custom block of " << x << "x" << y << endl;
            ei = new InterFrame(filepath, x, y);
        } else {
            ei = new InterFrame(filepath);
            cout << "InterFrame initialized" << endl;
        }

        if(decode){
            cout << "Decoding using InterFrame codec" << endl;
            ei->decode();
        }else {
            cout << "Encoding using InterFrame codec" << endl;
            ei->encode();
        }

    } else if (v1) {
        EncodingInterface *ei;
        if (custom_block) {
            cout << "Custom block not supported in v1. Ignoring." << endl;
        }

        if (decode) {
            cout << "Decoding using version 1..." << endl;
            ei = new IntraFrame(filepath);
            ei->decode();
        } else {
            cout << "Encoding using version 1..." << endl;
            ei = new IntraFrame(filepath);
            ei->encode();
        }

        return 0;
    }
}