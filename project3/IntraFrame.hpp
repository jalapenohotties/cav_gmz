#include "EncodingInterface.hpp"
#include "string"
#include <iostream>
#include "Video.hpp"
#include "Predict.hpp"

#ifndef PROJECT3_INTRAFRAME_HPP
#define PROJECT3_INTRAFRAME_HPP

using namespace std;

#define FILE_EXTENSION ".v1"

class IntraFrame : public EncodingInterface {

public:

    /**
     * Initialize IntraFrame encoder. Output to the same directory.
     * @param filepath to the file to encode
     */
    IntraFrame(string filepath);

    /**
     * Full process of encoding an video file using IntraFrame mechanism
     */
    virtual void encode() override;

    /**
     * Full process of decoding an video file using IntraFrame mechanism
     * Also creates a video an plays it at the end
     */
    virtual void decode() override;

    /**
     * Ginve a Mat that contains an image compute the residuals
     * @param mat with the residuals
     */
    void calc_Mat_residuals(Mat *mat);

    /**
     * Given a Mat that contains only but residuals reconstruct the original image
     * @param mat with the original image
     */
    void calc_Mat_original(Mat *mat);

private:
    string filepath;

    unsigned int frameCount;
};


#endif //PROJECT3_INTRAFRAME_HPP
