#ifndef PROJECT3_INTERFRAME_HPP
#define PROJECT3_INTERFRAME_HPP


#include "EncodingInterface.hpp"
#include "string"
#include <iostream>
#include "Video.hpp"
#include "Predict.hpp"

using namespace std;

#define FILE_EXTENSION ".v2"

class InterFrame : public EncodingInterface {

public:
    /**
     * Initialize IntraFrame encoder. Output to the same directory.
     * @param filepath to the file to encode
     */
    InterFrame(string filepath);

    InterFrame(string basic_string, int i, int i1);

    ~InterFrame();
    /**
     * Full process of encoding an video file using IntraFrame mechanism
     */
    virtual void encode() override;

    /**
     * Full process of decoding an video file using IntraFrame mechanism
     * Also creates a video an plays it at the end
     */
    virtual void decode() override;

private:
    string filepath;

    int block_width = 8;

    int block_height = 8;

    void calc_Mat_residuals(Mat *mat);

    void calc_Mat_original(Mat *mat);

    Mat * calculateResiduals(Mat *mat1, Mat *mat2);

    Mat * calculateOriginals(Mat *ref_frame, Mat *curr_frame);

    Vec3b calcVec3bDiff(Vec3b a, Vec3b b);

    Vec3b undo_calcVec3bDiff(Vec3b a, Vec3b residual);
};


#endif //PROJECT3_INTERFRAME_HPP
