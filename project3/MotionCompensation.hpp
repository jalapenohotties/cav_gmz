#ifndef PROJECT3_MOTIONCOMPENSATION_H
#define PROJECT3_MOTIONCOMPENSATION_H

#include <iostream>
#include <vector>
#include <ml.h>

using namespace std;
using namespace cv;

class MotionCompensation {
public:

    int DEFAULT_PERIODICITY = 2;
    int DEFAULT_BLOCK_SIZE = 8;

private:
    int search_width;
    int search_height;
};


#endif //PROJECT3_MOTIONCOMPENSATION_H
