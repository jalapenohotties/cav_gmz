#include <opencv2/opencv.hpp>
#include <iostream>
#include "Utils.hpp"

#ifndef PROJECT3_PREDICT_HPP
#define PROJECT3_PREDICT_HPP

using namespace cv;
using namespace std;

class Predict {

public:
    /**
     * Converts the given Mat to it's residual values
     * Residuals are already encoded in only positive numbers, ready for Golomb
     * @param p_mat Mat (image) of the predictor result
     */
    Mat calcResiduals(Mat *mat);

    /**
     * Given an Mat with residuals computes the original image.
     * @param p_mat Mat that contains the residuals
     */
    Mat calcOriginals(Mat *mat);

    /**
     * Computes the difference between two pixels and encodes it
     * to a positive number. Used to calculate the predictor error.
     * @param a Original pixel value
     * @param b Pixel value to compare (predicted value)
     * @return Pixel differences encoded in a positive number
     */
    Vec3b  calcVec3bDiff(Vec3b a, Vec3b b);

    /**
     * Inverse function of calcVec3bDiff, that restores the pixel value
     * by adding the saved error of the predictor.
     * @param a Predicted value
     * @param b Error of the predictor
     * @return Original pixel value
     */
    Vec3b  undo_calcVec3bDiff(Vec3b a, Vec3b b);

private:
    /**
     * Given adjacent values calculates the predicted value
     * @param a adjacent pixel at same line, one col before
     * @param b adjacent pixel one line above
     * @param c adjacent pixel one line above, one col before
     * @return The predicted value for all 3 components (RGB)
     */
    Vec3b predict_Vec3b(Vec3b a, Vec3b b, Vec3b c);


};


#endif //PROJECT3_PREDICT_HPP
