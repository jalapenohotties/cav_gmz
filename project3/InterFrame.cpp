
#include "InterFrame.hpp"

InterFrame::InterFrame(string filepath) : filepath(filepath) {
    cout << "InterFrame::InterFrame() initialized. input:" << filepath << endl;
    block_height = 512;
    block_width = 512;
}

InterFrame::InterFrame(string filepath, int width, int height) : filepath(filepath) {
    block_width = width;
    block_height = height;
}

InterFrame::~InterFrame() {}

void InterFrame::encode() {
    cout << "encode()" + filepath << endl;
    Video video = Video(filepath);

    string outFilename = filepath + FILE_EXTENSION;
    Utils utils = Utils(outFilename, "w");

    Mat *current_frame, *previous_frame;

    /*Header Section*/
    utils.write_value(video.getCols());
    utils.write_value(video.getRows());
    utils.write_value(video.getFPS());
    utils.write_value(video.nrFrames());

    /*Encoding*/
    current_frame = video.getNext();
    previous_frame = current_frame;

    /*First Frame: I-Frame encoding (Intra frame)*/
    //calc_Mat_residuals(previous_frame);
    utils.mat_to_bs(previous_frame);

    unsigned int frameCounter = 0;
    while (video.hasNext()) {

        cout << "encoding frame()" << frameCounter << endl;
        current_frame = video.getNext();

        /*P-Frame logic*/
        //calc_Mat_residuals(current_frame);
        Mat *result = calculateResiduals(previous_frame, current_frame);
        utils.mat_to_bs(result);

        previous_frame = current_frame;

        frameCounter++;
    }

    utils.close();
}

void InterFrame::decode() {

    string inFile = filepath;
    Utils utils = Utils(inFile, "r");

    unsigned int nCols = utils.read_value();
    unsigned int nRows = utils.read_value();
    unsigned int fps = utils.read_value();
    unsigned int frames = utils.read_value();


    Video video = Video(nCols, nRows, fps);
    Mat *previous_frame = new Mat(nCols, nRows, CV_8UC3);

    /*I-Frame decoding (Intra Coding)*/
    utils.bs_to_mat(previous_frame);

    //Mat *firstFrame;
    //*firstFrame = previous_frame->clone();
    //calc_Mat_original(firstFrame);
    //video.addFrame(firstFrame);

   // imshow("firstFrame ", *previous_frame);
   // waitKey(0);

    unsigned int frameCounter = 0;
    while (frameCounter < frames) {

        cout << "decoding frame: " << frameCounter << endl;

        Mat *current_frame = new Mat(nCols, nRows, CV_8UC3);

        utils.bs_to_mat(current_frame); // frame de residuais


        //imshow("primeira ", *previous_frame);
        //imshow("segunda ", *current_frame);
        //waitKey(0);

        Mat *sum_result = calculateOriginals(previous_frame, current_frame);

        //calc_Mat_original(sum_result);

        video.addFrame(sum_result);

        //imshow("decoded", *sum_result);
        //waitKey(0);

        previous_frame = sum_result;
        frameCounter++;
    }

    cout << "playing" << endl;

    int i = 0;
    while (video.hasNext()) {
        cout << "\r" << "IntraFrame::decode(): playing frame " << i++ + 1 << "/" << frames << "..." << flush;
        Mat *tmp = video.getNext();
        imshow("Display Image", *tmp);
        if (waitKey((int) (1.0 / fps * 1000)) >= 0) break;
    }

}

void InterFrame::calc_Mat_residuals(Mat *mat) {
    Predict p = Predict();
    p.calcResiduals(mat);
}

void InterFrame::calc_Mat_original(Mat *mat) {
    Predict p = Predict();
    p.calcOriginals(mat);
}

Mat *InterFrame::calculateResiduals(Mat *ref_frame, Mat *curr_frame) {
    Mat *result = new Mat(ref_frame->rows, ref_frame->cols, ref_frame->type());
    *result = ref_frame->clone();

    for (int i = 0; i < ref_frame->cols; i += block_height) {
        for (int j = 0; j < ref_frame->rows; j += block_width) {
            for (int ii = i; ii < i + block_height; ii++) {
                for (int jj = j; jj < j + block_width; jj++) {

                    if (ii > ref_frame->rows || jj > ref_frame->cols)
                        continue;

                    result->at<Vec3b>(ii, jj) = calcVec3bDiff(ref_frame->at<Vec3b>(ii, jj),
                                                              curr_frame->at<Vec3b>(ii, jj));
                }
            }
            //imshow("Display Image1", *ref_frame);
            //imshow("Display Image2", *curr_frame);
            //imshow("Display Image3", *result);
            //waitKey(0);
        }
    }
    return result;
}

Mat *InterFrame::calculateOriginals(Mat *ref_frame, Mat *curr_frame) {

    Mat *result = new Mat(ref_frame->cols, ref_frame->rows, CV_8UC3);

    for (int i = 0; i < ref_frame->cols; i += block_height) {
        for (int j = 0; j < ref_frame->rows; j += block_width) {
            for (int ii = i; ii < i + block_height; ii++) {
                for (int jj = j; jj < j + block_width; jj++) {

                    if (ii > ref_frame->rows || jj > ref_frame->cols)
                        continue;

                    result->at<Vec3b>(ii, jj) = undo_calcVec3bDiff(ref_frame->at<Vec3b>(ii, jj),
                                                                   curr_frame->at<Vec3b>(ii, jj));
                }
            }

            //imshow("Display Image1", *ref_frame);
            //imshow("Display Image2", *curr_frame);
            //imshow("Display Image3", *result);
            //waitKey(0);
        }
    }

    return result;
}

Vec3b InterFrame::undo_calcVec3bDiff(Vec3b a, Vec3b residual) {

    Vec3b result;

    for (int i = 0; i < 3; i++)
        result[i] = a[i] + residual[i]  >= 255 ? a[i] - residual[i] :  a[i] + residual[i];


    return result;

}

Vec3b InterFrame::calcVec3bDiff(Vec3b a, Vec3b b) {

    Vec3b result;

    for (int i = 0; i < 3; i++)
        result[i] = b[i] - a[i] < 0 ? a[i] - b[i] : b[i] - a[i];

    return result;
}