#include <sndfile.h>
#include "Utils.hpp"

Utils::Utils(string outFile, std::string m) {
    cout << "Utils::Utils(): linked to file:" << outFile << endl;

    if (m == "r") {
        bs = new BitStream(outFile, "r");
    } else {
        bs = new BitStream(outFile, "w");
    }


}

void Utils::mat_to_bs(Mat *mat) {

    // flatten the matrix to an array and count occurences of each value
    counter = map<unsigned int, unsigned int>();

    std::vector<uchar> array;
    for (int i = 0; i < mat->rows; i++) {
        for (int j = 0; j < mat->cols; j++) {
            for (int c = 0; c < 3; c++) {
                array.push_back(mat->at<Vec3b>(i, j)[c]);
                counter[(unsigned int) mat->at<Vec3b>(i, j)[c]]++;
            }
        }
    }

    // now that its flattened find ideal M
    //int m = find_ideal_M();

    GolombEncoder golomb = GolombEncoder(bs, GOLOMB_M_HINT);

    for (unsigned int i = 0; i < array.size(); i++)
        golomb.encode(array[i]);

}


void Utils::bs_to_mat(Mat *mat) {

    GolombDecoder golomb = GolombDecoder(bs, GOLOMB_M_HINT);
    Vec3b v;
    for (int i = 0; i < mat->rows; i++) {
        for (int j = 0; j < mat->cols; j++) {

            v[0] = (uchar) golomb.decode();
            v[1] = (uchar) golomb.decode();
            v[2] = (uchar) golomb.decode();
            mat->at<Vec3b>(i, j) = v;

        }
    }
}

void Utils::write_value(unsigned int val) {
    GolombEncoder golomb = GolombEncoder(bs, GOLOMB_M_HINT);
    golomb.encode(val);
}

unsigned int Utils::read_value() {
    GolombDecoder golomb = GolombDecoder(bs, GOLOMB_M_HINT);
    return golomb.decode();
}


unsigned int Utils::find_ideal_M() {

    unsigned int m = 0;
    ofstream tmp_file;

    tmp_file.open(string(TMP_FILE_PATH));

    for (auto it: counter)
        tmp_file << it.first << "\t" << it.second << "\n";

    tmp_file.close();


    FILE *pipe = popen(GNUPLOT_POPEN, "w");
    if (!pipe)
        throw runtime_error("popen() failed!");
    try {
        fprintf(pipe, string("set print '" + string(TMP_FILE_PATH) + ".m'\n").c_str());
        fprintf(pipe, "set fit quiet\n");
        fprintf(pipe, "f(x) = 100000 * (1-p) * p**x\n");
        fprintf(pipe, string("fit [x=0:150] f(x) '" + string(TMP_FILE_PATH) + "' using 1:2 via p\n").c_str());
        fprintf(pipe, "print ceil(-(1/(log(p)/log(2))))\n");
        fprintf(pipe, "exit\n");
        fflush(pipe);
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);

    // once optimum M is determined, read result and GolombToFile p_differences using Golomb
    fstream gnuplot_res(string(TMP_FILE_PATH) + ".m", std::ios_base::in);
    gnuplot_res >> m; // read optimum golomb for channel
    cout << "Utils::find_ideal_M(): m result:" << m << endl;
    gnuplot_res.close();

    return m;

}

void Utils::close() {
    bs->close();
}


