#include <getopt.h>
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <iostream>
#include <fstream>

using namespace cv;
using namespace std;

void print_usage()
{
	cout << "Usage: DisplayVideo [-h] [-v] [-f fileName] [-c camId]\n";
}

int main(int argc, char** argv )
{
	int fps;
	int option;
	Mat image;
	VideoCapture cam;
	ifstream myfile;
	string inputFileName;
	int camId = 0;
	bool verbose = false;

	while ((option = getopt(argc, argv,"vhc:f:")) != -1) {
		switch (option)
		{
		case 'f':
			inputFileName = optarg;
			break;
		case 'c':
			camId = atoi(optarg);
			break;
		case 'h':
			print_usage();
			exit(EXIT_FAILURE);
			break;
		case 'v':
			verbose = true;
			break;
		default:
			print_usage();
			exit(EXIT_FAILURE);
		}
	}

	if(inputFileName.size() > 0)
	{
		myfile.open(inputFileName);
		if (!myfile.is_open())
		{
			cerr << "Error opening file\n";
			return -1;
		}

		string line;
		int nCols, nRows, type;
		getline (myfile,line);
		if(verbose) cout << line << endl;
		istringstream(line) >> nCols >> nRows >> fps >> type;
		image = Mat(Size(nCols, nRows), CV_8UC3);
	}
	else
	{
		cam.open(camId);// Open input
		if (!cam.isOpened())
		{
			cout  << "Could not open camera: " << std::endl;
			return -1;
		}
		fps = cam.get(CV_CAP_PROP_FPS);
	}


	while(true)
	{
		if(inputFileName.size() > 0)
		{
			if(!myfile.read((char*)image.data, image.cols * image.rows * image.channels())) break;
		}
		else
		{
			cam >> image;              // read
		}

		if (image.empty()) break;         // check if at end

		imshow("Display Image", image);

		if(waitKey((int)(1.0 / fps * 1000)) >= 0) break;
	}

	if(myfile.is_open()) myfile.close();

	return 0;
}
