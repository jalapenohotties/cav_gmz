#include "Video.hpp"

Video::Video(unsigned int nCols, unsigned int nRows, unsigned int fps) : nCols(nCols),
                                                                         nRows(nRows),
                                                                         fps(fps) {

    arrFrames = std::vector<cv::Mat *>();
    pos = 0;
}

Video::Video(std::string filepath) {

    pos = 0;
    arrFrames = std::vector<cv::Mat *>();
    std::ifstream file;

    std::cout << filepath << std::endl;

    if (filepath.size() > 0) {
        file.open(filepath);
        if (!file.is_open()) {
            std::cerr << "Video::Video(): Error opening file\n";
            exit(-1);
        }

        std::string line;
        getline(file, line);
        std::cout << "Video::Video(): RGB line:" << line << std::endl;
        std::istringstream(line) >> nCols >> nRows >> fps >> type;
    }

    while (true) {
        cv::Mat *image = new cv::Mat;
        image->create(cv::Size(nCols, nRows), CV_8UC3);

        if (!file.read((char *) image->data, image->cols * image->rows * image->channels())) break;

        if (image->empty()) break;         // check if at end

        // display the video while reading
        //imshow("Display Image", *image);
        //if (waitKey((int) (1.0 / fps * 1000)) >= 0) break;

        arrFrames.push_back(image);
    }

    if (file.is_open()) file.close();

    std::cout << "Video::Video(): loaded " << arrFrames.size() << " frames into memory" << std::endl;

}

void Video::addFrame(cv::Mat *mat) {
    arrFrames.push_back(mat);
}


bool Video::hasNext() {
    return pos != arrFrames.size();
}

cv::Mat *Video::getNext() {
    return arrFrames[pos++];
}

unsigned int Video::nrFrames() {
    return arrFrames.size();
}

unsigned int Video::getFPS() {
    return fps;
}

unsigned int Video::getCols() {
    return nCols;
}

unsigned int Video::getRows() {
    return nRows;
}
