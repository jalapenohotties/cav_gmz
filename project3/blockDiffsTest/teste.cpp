
#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <set>
#include "teste.h"
#include <list>

using namespace cv;
using namespace std;


int match_method = CV_TM_CCOEFF_NORMED;

struct scale_found {
    double maxval;
    Point maxloc;
    double r = 1;
    double fx_fy = 1;
};
scale_found found;



//percorre a imagem toda testando varios tamanhos contra a frame mais pequena, e devolve uma nova frame com o tamanho ideal
Mat multiScale(Mat *disp, Mat *templ_img) {
    Mat resized;
    Mat result;
    double minim = 0.2;
    double maxim = 1;
    double samples = 40;
    double steps = (maxim - minim) / samples;
    float i;

    for (i = maxim; i > minim; i -= steps) {
        cv::resize(*disp, resized, Size(), i, i);
        if (resized.rows < (*templ_img).rows || resized.cols < (*templ_img).cols) {
            break;
        }
        int result_cols = resized.cols - (*templ_img).cols + 1;
        int result_rows = resized.rows - (*templ_img).rows + 1;

        result.create(result_cols, result_rows, CV_32FC1);

        matchTemplate(resized, (*templ_img), result, match_method);
        if (match_method == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED) {
            subtract(255, result, result);
        }
        double minVal, maxVal;
        Point minLoc, maxLoc;
        minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());
        if (maxVal > found.maxval) {
            found.maxval = maxVal;
            found.maxloc = maxLoc;
            found.fx_fy = i;
            found.r = disp->cols / (double) resized.cols;
        }

    }
    cv::resize(*disp, resized, Size(), found.fx_fy, found.fx_fy);
    return resized;
}

void templateMatching(Mat *imagem, Mat *templImg){

    Mat *templ_aux = templImg;
    Mat *image = imagem;

    Mat displ;
    Mat result;



    /*limpar valores antigos*/
    scale_found foud_aux;
    found = foud_aux;
    /*criar uma copia, preservar a original*/
    displ = image->clone();

    Mat aux_img = image->clone();
    Mat resized = displ.clone();


    //if (multiScale_bool) {
    if (true) {
        resized = multiScale(&displ, templ_aux);
    }

    //displ = image.clone();

    //result.release();
    /*criar a Mat de resultado*/
    int result_cols = resized.cols - templ_aux->cols + 1;
    int result_rows = resized.rows - templ_aux->rows + 1;

    result.create(result_cols, result_rows, CV_32FC1);

    /*popular a Mat resultado, fazendo match*/
    matchTemplate(resized, *templ_aux, result, match_method);
    normalize(result, result, 0, 1, NORM_MINMAX, -1, Mat());

    /* For SQDIFF and SQDIFF_NORMED,
     *the best matches are lower values.
     *For all the other methods, the higher the better*/
    if (match_method == CV_TM_SQDIFF || match_method == CV_TM_SQDIFF_NORMED) {
        subtract(255, result, result);
    }


    Point maxLoc2;
    int auxMax;
    while (true) {
        double minVal, maxVal;
        Point minLoc, maxLoc;
        minMaxLoc(result, &minVal, &maxVal, &minLoc, &maxLoc, Mat());


        //escolhe o valor mais correspondente e armazena para posterior uso
        if (auxMax<maxVal){
            auxMax=maxVal;
            maxLoc2=maxLoc;
        }
        //threshold maximo, abaixo desse valor nao queremos considerar.
        if (maxVal < 0.99) { //[0-1]
            break;
        }


        /*rectangulo preenchido*/ //invalida passagens repetidas
        rectangle(result, maxLoc, Point(maxLoc.x + templ_aux->cols, maxLoc.y + templ_aux->rows), Scalar(0, 0, 255),
                  CV_FILLED, 8, 0);
    }

    //rectangulo para visualizacao
    rectangle(aux_img, maxLoc2 * found.r,
              Point((maxLoc2.x + templ_aux->cols) * found.r, (maxLoc2.y + templ_aux->rows) * found.r),
              Scalar(0, 0, 255), 2,
              8,
              0);

    displ = aux_img.clone();

    namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
    imshow("Display window", displ);
    waitKey(0);

}

int main(__attribute__((unused)) int argc, __attribute__((unused)) char **argv) {

    Mat image;


    image = imread("./blockDiffsTest/braco.png", CV_LOAD_IMAGE_COLOR);

    if (image.empty()) {
        cout << "assert this" << endl;
    }

    Mat templ_aux = imread("./blockDiffsTest/template_braco_resized.png", CV_LOAD_IMAGE_COLOR);//(*iter).clone();

    if (templ_aux.empty()) {
        cout << "assert this" << endl;
    }

    templateMatching(&image,&templ_aux);

}


int main2(__attribute__((unused)) int argc, __attribute__((unused)) char **argv) {

    Mat image1;
    image1 = imread("./blockDiffsTest/background.png", CV_LOAD_IMAGE_COLOR);   // Read the file

    if (!image1.data)                              // Check for invalid input
    {
        cout << "Could not open or find the image" << std::endl;
        //return -1;
    }
    Mat image2;
    image2 = imread("./blockDiffsTest/braco.png", CV_LOAD_IMAGE_COLOR);   // Read the file

    if (!image2.data)                              // Check for invalid input
    {
        cout << "Could not open or find the image2" << std::endl;
        //return -1;
    }

    //as duas mats sao dadas como arg

    //variaveis arbitrarias a passar como arg
    int blockLines = 99;
    int blockColumns = 99;
    std::list<Mat> blocksMat; //matriz de blocos a devolver

    Mat *resultado;//matriz aux para inserir na lista
    //nao tenho certeza se na lista e' push back ou fron ou outro.
    int i = 0, j = 0, ii = 0, jj = 0;
    //percorre a imagem tendo em conta a multiplicidade do tamanho do bloco e da imagem
    for (i = 0; i < (image1.rows / blockLines) * blockLines; i += blockLines) {
        for (j = 0; j < (image1.cols / blockColumns) * blockColumns; j += blockColumns) {
            resultado = new Mat(image1.rows, image1.cols, CV_8UC3, Scalar(0, 0, 0));//nova mat aux

            //faz a diferenca das duas imagens e coloca na matriz aux
            for (ii = 0; ii < blockColumns; ii++) {
                for (jj = 0; jj < blockColumns; jj++) {
                    resultado->at<Vec3b>(ii, jj) = image2.at<Vec3b>(i + ii, j + jj) - image1.at<Vec3b>(i + ii, j +
                                                                                                               jj); //diferenca entre as duas imagens, vec3b = B,G,R
                }
            }
            blocksMat.push_back(*resultado); //inserir na list
        }
    }

    //restos, quando os blocos na sao multiplos do tamanho da imagem
    resultado = new Mat(image1.rows, image1.cols, CV_8UC3, Scalar(0, 0, 0));
    for (ii = 0; ii < image1.rows % blockLines; ii++) {
        for (jj = 0; jj < image1.cols % blockColumns; jj++) {
            resultado->at<Vec3b>(ii, jj) = image2.at<Vec3b>(i + ii, j + jj) - image1.at<Vec3b>(i + ii, j + jj);
        }
    }
    blocksMat.push_back(*resultado); //inserir na list


    //iterador para teste
    cout << blocksMat.size() << endl;
    std::list<Mat>::iterator it;
    int x = 0;
    for (it = blocksMat.begin(); it != blocksMat.end(); ++it) {
        cout << x << endl;
        x++;
        namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
        imshow("Display window", *it);
        waitKey(0);
    }

    return 0;

}
