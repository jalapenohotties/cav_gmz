#include <string>


#ifndef PROJECT3_ENCODINGINTERFACE_H
#define PROJECT3_ENCODINGINTERFACE_H


class EncodingInterface {

public:

    virtual void encode()= 0;
    virtual void decode()= 0;

};


#endif //PROJECT3_ENCODINGINTERFACE_H
