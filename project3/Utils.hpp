#include <opencv2/opencv.hpp>
#include "BitStream.h"
#include "Golomb.hpp"

#ifndef PROJECT3_UTILS_HPP
#define PROJECT3_UTILS_HPP

using namespace cv;
using namespace std;

#define TMP_FILE_PATH "/tmp/gnuplot.tmp"
#define GNUPLOT_POPEN "/usr/bin/gnuplot -persist 2> /dev/null"

#define GOLOMB_M_HINT 3


class Utils {

public:

    /**
     * Initializes an Util for a file.
     * Deals with Golomb and Bistreams
     * Note: Remember to .close()
     * @param outFile to use
     * @param mode "r" for read, "w" for write
     */
    Utils(string outFile, string mode);

    /**
     * For a Mat, computes the ideal M-Golomb value and writes it to a file
     * @param mat to write to file using Golomb and BitStreams
     */
    void mat_to_bs(Mat *mat);

    /**
     * Reads from a file using BitStream an reconstructs the values of a Mat
     * @param mat to reconstruct
     */
    void bs_to_mat(Mat *mat);

    /**
     * Writes arbitrary values to the file using Golomb/BitStream
     * @param val to write
     */
    void write_value(unsigned int val);

    /**
     * Reads an arbitrary value from the file
     * @return value read
     */
    unsigned int read_value();

    /**
     * Closes the access to the file
     */
    void close();

private:
    unsigned int find_ideal_M();

    map<unsigned int, unsigned int> counter;

    BitStream *bs;

};


#endif //PROJECT3_UTILS_HPP
