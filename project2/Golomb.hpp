#ifndef PROJECT2_GOLOMB_H
#define PROJECT2_GOLOMB_H

#include <math.h>
#include <cstdio>
#include <bitset>
#include <stdlib.h>
#include <string>
#include <boost/dynamic_bitset.hpp>

#include "BitStream.h"

using namespace std;
using namespace boost;


class Golomb {

public:

    /**
     * \brief Standard constructor. The use of such constructor requires setTunable() and setBitStream() to be invoked afterwards.
     */
    Golomb();

    /**
     * \brief Main constructor, receives a BitStream as parameter. The latter is later on used for encode and decode.
     * @param bs
     * @return
     */
    Golomb(BitStream* bs);

    /**
     * \brief Golomb class destructor.
     */
    ~Golomb(void);

    /**
     * \brief Encoding method. This method requires BitStream to be initialized. Otherwise an error message will be returned and the
     * operation will be ignored.
     * @param n The parameter which will be encoded.
     */
    void encode_toBitStream(unsigned int n);

    /**
     * \brief Decode method. This method requires BitStream to be initialized. Otherwise an error message will be returned and the operation
     * will be ignored. No parameter needed, the value to be decoded is read directly from the Bitstream.
     */
    unsigned int decode();

    /**
     * \brief This method is esential whenever the tunable must be changed. Additional calculus are performed within, such as the cutoff and the code
     * length.
     * @param tunable - The new m parameter which will divide n on the encode and decode processes.
     */
    void setTunable(unsigned int tunable);

    /**
     * \brief This method allows the programmer to set a bitstream, through a pointer. This method is necessary whenever the source/destination file changes and/or
     * the standard constructor is used.
     * @param bs
     */
    void setBitStream(BitStream* bs);

private:

    /**
     * \brief A pointer to a BitStream received on a higher level.
     */
    BitStream *p_bs;

    /**
     * \brief The tunable parameter m.
     */
    unsigned int m;

    /**
     * \brief The size of the code, which varies according to the value of m. Many references call this variable "b".
     */
    unsigned int code_length;

    /**
     * \brief The cutoff, which is the value compared to the remainder whenvever m isn't a power of two. This value is,
     * according to certain conditions, encoded with the remainder.
     */
    unsigned int cutoff;

    /**
     * \brief Boolean flag that provides direct access whether the tunable parameter m is a power of two.
     */
    bool mPowerOfTwo;

    /**
     * \brief Boolean flag that tells wether the m parameter is initialized.
     */
    bool tunableSet;

    /**
     * \brief Boolean flag that is either true or false acording to the pointer of a bitStream.
     */
    bool bsSet;
};

#endif //PROJECT2_GOLOMB_H
