#ifndef PROJECT2_CAVDECODER_H
#define PROJECT2_CAVDECODER_H

#include "libsnd_wrapper.h"
#include "BitStream.h"
#include "Golomb.hpp"
#include "Predictor.hpp"


class CAVdecoder {

public:
    /**
     * Decodes an CAV file. Auto detects used encoding method
     * @param path_in to file to decode
     * @param path_out to file to save the decoded file
     * @return
     */
    CAVdecoder(const char *path_in, const char *path_out);

private:
    const char *path_CAV_in;
    const char *path_WAV_out;
    short *p_predict;
    int redundancy_bits;
    libsnd_wrapper *wavFileOut;
    vector<short> *p_differences;

    void decode();

    short *decodeLossy(short *p_sample, int desloc, unsigned short channels);

    short *decodeChannelRedundancy(short *p_sample, unsigned short channels);

    short *decodeTemporalRedundancy(short *decodedVal, Predictor *preds, unsigned short channels);

    short * decodeNegativePositiveNumber(unsigned int * decodedVal, unsigned short channels);

    void init(int numFrames);
};


#endif //PROJECT2_CAVDECODER_H
