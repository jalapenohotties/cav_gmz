#! /usr/bin/gnuplot --persist

f(x) = (1-p) * p**x
fit [x=0:2500] f(x) 'dados.csv' using 1:3 via p
plot "dados.csv" using 1:3, (1-p) * p**x

