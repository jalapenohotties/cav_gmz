#include <climits>
#include "BitStream.h"

BitStream::BitStream(const char *filePath) {
    file.open(filePath,  ios::in | ios::out | ios::app | ios::binary);

    // pull the pointer to the begin of the file
    file.seekg(0, ios::beg);

    // set in position and out position to zero
    inpos = 0;
    outpos = CHAR_SIZE;
    // initializes at zeros
    bufout = bitset<CHAR_SIZE>(0);
}

unsigned int BitStream::getBit() {
    if (inpos == 0) {
        char c;
        file.read(&c, 1);
        bufin = bitset<CHAR_SIZE>(c);
        inpos = CHAR_SIZE;
    }
    return (unsigned int) bufin[--inpos];
}

boost::dynamic_bitset<> BitStream::get_nBit(unsigned int number_of_bits) {
    boost::dynamic_bitset<> tmp(number_of_bits);

    for (int i = number_of_bits - 1; i >= 0; i--) {
        tmp[i] = getBit();
    }
    return tmp;
}

void BitStream::writeBit(unsigned int bit) {
    if (outpos == 0) {
        file.put(bufout.to_ulong());
        bufout.reset();
        outpos = CHAR_SIZE;
    }

    bufout[--outpos] = bit;
}

void BitStream::write_nBit(boost::dynamic_bitset<> outBitSet) {
    for (int i = outBitSet.size() - 1; i >= 0; i--) {
        writeBit(outBitSet[i]);
    }
}

void BitStream::write_value(uint value, uint nBits) {
    boost::dynamic_bitset<> outBitSet(nBits, value);
    write_nBit(outBitSet);
}

void BitStream::flushAndClose() {
    if (outpos != CHAR_SIZE) {
        file.put(bufout.to_ulong());
    }
    file.flush();
    file.close();
}