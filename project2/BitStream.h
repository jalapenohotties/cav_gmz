#ifndef PROJECT2_BITSTREAM_H
#define PROJECT2_BITSTREAM_H

#include "BitStream.h"
#include <iostream>
#include <fstream>
#include <bitset>
#include <cstring>
#include <boost/dynamic_bitset.hpp>

typedef unsigned int uint;

// Minimal unit to read and write when using ifstream / ofstream
#define CHAR_SIZE sizeof(char)*8


using namespace std;


class BitStream {


public:

    /**
     * Initializes an abstraction for reading and writing bits from a buffer
     * @param filePath of the file
     */
    BitStream(const char *filePath);


    /**
     * Gets a bit from the stream
     * @return next bit
     */
    unsigned int getBit();


    /**
    * \brief Gets n bits from the stream
    * @param number_of_bits to get
    * @return corresponding bits
    */
    boost::dynamic_bitset<unsigned long, allocator<unsigned long>> get_nBit(unsigned int number_of_bits);


    /**
    * \brief Write a bit to the stream
    * @param bit to write
    */
    void writeBit(unsigned int bit);

    /**
     * Writes an value and the corresponding number of bits
     * @param value to write
     * @param nBits number of bits of the value
     */
    void write_value(uint value, uint nBits);

    /**
     * Flushes cache to file and closes the stream
     */
    void flushAndClose();


private:
    fstream file;

    /**
     * buffer positions for respective in or out stream
     */
    unsigned int inpos;
    unsigned int outpos;

    /**
     * bit buffers
     */
    bitset<CHAR_SIZE> bufin;
    bitset<CHAR_SIZE> bufout;

    /**
     * Checks if the input stream was opened and buffer initialized
     */
    void checkRead();

    bool alreadyRead;

    /**
     * Checks if the stream was already opened
     */
    void checkwrote();

    bool alreadyWrote;

    /**
     * Write n bits to the stream
     * @param outBitSet to write
     */
    void write_nBit(boost::dynamic_bitset<> outBitSet);
};


#endif //PROJECT2_BITSTREAM_H