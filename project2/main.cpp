#include <iomanip>
#include "BitStream.h"
#include "WAVencoder.h"
#include "CAVdecoder.h"

using namespace std;

void printMenu() {
    cout << "CAV wave encoder" << endl;
    cout << endl;
    cout << "-c\t\tlossless encode/decode using channel redundancy" << endl;
    cout << "-l [bits]\tlossy encode discarding n [bits]" << endl;
    cout << "-d [file]\tDecode file" << endl;
    cout << "-s\t\tDisplay stats" << endl;
    cout << "-h\t\tDisplay this help" << endl;
    cout << endl;
    cout << "Examples:" << endl;
    cout << "1. Encode to cav format using temporal redundancy:" << endl;
    cout << "\t./cav sample01.wav" << endl;
    cout << endl;
    cout << "2. Encode to cav format using channel redundancy" << endl;
    cout << "\t./cav -c sample01.wav" << endl;
    cout << endl;
    cout << "3. Decode cav format to WAV:" << endl;
    cout << "\t./cav -d sample01.cav" << endl;

}

int main(int argc, char **argv) {


    if (argc == 1)
        cout << "Usage: -h for help" << endl;

    // Read options from arguments
    extern int opterr; // suppress getopt error message
    opterr = 0;

    int lossy_bits = 0;
    bool decode = false;
    bool stats = false;
    bool channel_redundancy = false;

    char c;
    while ((c = getopt(argc, argv, "dschl:")) != -1)
        switch (c) {
            case 'd':
                decode = true;
                break;
            case 'c':
                channel_redundancy = true;
                break;
            case 'h':
                printMenu();
                return 0;
            case 's':
                stats = true;
                break;
            case 'l':
                if ((lossy_bits = stoi(optarg)) <= 0) {
                    cerr << "Lossy bits '" << optarg << "' is invalid. Must be number >0" << endl;
                    return 1;
                }
                break;
            default:
                cout << "Invalid arguments" << endl;
                cout << "Run with -h for help" << endl;
                return 0;
        }


    int index;
    string filepath = "";
    for (index = optind; index < argc; index++) {
        filepath = string(argv[index]);
    }

    if (filepath.length() == 0) {
        cout << "Include file to encode/decode" << endl;
        return 1;
    }

    if (!decode) {

        // encode a file to .cav
        WAVencoder *encoder;

        clock_t begin = clock();
        string tmp = string(filepath + string(".cav"));

        if (lossy_bits == 0) { // encoding lossless

            encoder = new WAVencoder(filepath.c_str(), tmp.c_str(), channel_redundancy);

        } else { // lossy encode
            encoder = new WAVencoder(filepath.c_str(), string(filepath + string(".cav")).c_str(), lossy_bits);
        }

        clock_t end = clock();

        if (stats) {
            cout << "Encoding time: " << (double) (end - begin) / CLOCKS_PER_SEC << endl;
            encoder->printStats();
        }

    } else {
        // decode back to .wav
        CAVdecoder *decoder = new CAVdecoder(filepath.c_str(), string(filepath + string(".wav")).c_str());
    }

    return 0;

}
