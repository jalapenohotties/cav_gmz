#ifndef PROJECT2_PREDICTOR_H
#define PROJECT2_PREDICTOR_H

#include <iostream>
#include <vector>
#include <boost/circular_buffer.hpp>

#define ORDER 3

class Predictor {

public:

    /**
     * Initializes an predictor
     * @param number_channels number of channels to predict independitally
     * @return a new preditor
     */
    Predictor(int number_channels);

    ~Predictor();

    /**
     * Given an sample for a channel predicts the next sample
     * @param sample from wich to predict the next
     * @param channel to use
     * @return prediction of the next sample
     */
    short predictor_3n(short sample, unsigned short channel);

private:
    /**
     * sample buffer to use to predict the next
     */
    boost::circular_buffer<short> *predictor_buffer;
};


#endif //PROJECT2_PREDICTOR_H
