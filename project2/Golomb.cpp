//
// Created by mmachado on 19/10/16.
//

#include "Golomb.hpp"

#define quotient(n, m) floor(n/m);
#define remainder(n, m) n%m;

Golomb::Golomb() {
    m = 0;
    tunableSet = false;
    bsSet = false;
}

Golomb::Golomb(BitStream *bs) {
    p_bs = bs;
    m = 0;
    tunableSet = false;
    bsSet = true;
}

Golomb::~Golomb(void) {
    delete p_bs;
}

/*Sets (& Gets)*/
void Golomb::setTunable(unsigned int tunable) {
    if (tunable >= 0) {
        if (tunable == m) {
            //cout << "Bad Parameter: Trying to set value equal to current tunable."
            //        "\nIgnoring command..." << endl;
            return;
        }

        m = tunable;
        mPowerOfTwo = ((m != 0) && ((m & (m - 1)) == 0));

        if (mPowerOfTwo) code_length = log2(m);
        else code_length = ceil(log2(m));

        cutoff = pow(2, code_length) - m;
        tunableSet = true;
    } else
        cerr << "Bad Parameter (Golomb.cpp) - Given tunable parameter is invalid:"
                "\nNegative values are not accepted." << endl;
}

void Golomb::setBitStream(BitStream *bs) {
    p_bs = bs;
    bsSet = true;
}


void Golomb::encode_toBitStream(unsigned int n) {

    if (!tunableSet)
        cerr << "Bad parameter was not set to golomb while trying to encode"
                "\nPlease set the paramether with setTunable()" << endl;

    if (!bsSet) cerr << "No given bitstream! Impossible to encode without source bitstream" << endl;

    unsigned int quot = (unsigned int) quotient(n, m);
    unsigned int rem = remainder(n, m);

    int count = 0;
    while (count < quot) {
        p_bs->writeBit(1);
        count++;
    }
    p_bs->writeBit(0);

    if (mPowerOfTwo) p_bs->write_value(rem, log2(m));
    else if (rem < cutoff) p_bs->write_value(rem, code_length - 1);
    else p_bs->write_value(rem + cutoff, code_length);
}


unsigned int Golomb::decode() {

    if (!tunableSet)
        cerr << "Bad parameter was not set to golomb while trying to encode"
                "\nPlease set the paramether with setTunable()" << endl;

    if (!bsSet)
        cerr << "No BitStream provided! Impossible to decode without source bitstream" << endl;

    unsigned int q = 0, r = 0;

    while (p_bs->getBit() != 0) q++;

    if (mPowerOfTwo) r = (unsigned int) p_bs->get_nBit(code_length).to_ulong();
    else if ((r = (unsigned int) p_bs->get_nBit(code_length - 1).to_ulong()) >= cutoff) {
        r = ((r << 1) | p_bs->getBit()) - cutoff;
    }

    return q * m + r;
}