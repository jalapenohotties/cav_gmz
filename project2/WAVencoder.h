#ifndef PROJECT2_WAVencoder_H
#define PROJECT2_WAVencoder_H

#include <bits/unique_ptr.h>
#include "iostream"
#include "cstdlib"
#include "map"
#include <vector>
#include <string.h>
#include <iomanip>
#include "BitStream.h"
#include "libsnd_wrapper.h"
#include "Predictor.hpp"
#include <assert.h>

#define GNUPLOT_POPEN "/usr/bin/gnuplot -persist 2> /dev/null"
#define TMP_FILE_PATH "/tmp/gnuplot.tmp."
#define PRECISION 10
#define STATS_PRECISION 3

using namespace std;

class WAVencoder {

public:

    /**
     * Lossless encodes an WAV to CAV format
     * @param path_in of the file WAV to convert
     * @param path_out of the encoded file in CAV format
     * @param channel_redundancy use channel redundancy method or not
     */
    WAVencoder(const char *path_in, const char *path_out, bool channel_redundancy);

    /**
     * Lossy encodes an WAV to CAV format
     * @param path_in of the file WAV to convert
     * @param path_out of the encoded file in CAV format
     * @param redundancy_bits bits to discard upon lossy compression
     */
    WAVencoder(const char *path_in, const char *path_out, int redundancy_bits);

    /**
     * Print stats and displays GNU Plot graph
     */
    void printStats();

private:
    const char *path_in;
    const char *path_out;
    bool channel_redundancy;
    int redundancy_bits;

    libsnd_wrapper *wavFileIn;
    map<short, unsigned int> *p_histogram_before;
    map<short, double> *p_histogram_after;
    vector<unsigned int> *p_differences;

    short *p_predict;

    BitStream *bs;

    // TODO: convert to unique_ptr
    //unique_ptr<SNDFILE> p_soundFile;
    //unique_ptr<SF_INFO> soundInfoIn;
    //unique_ptr<map<short, unsigned int>> histogram;

    string filepath(int c);

    int *calcOptimalM();

    void init();

    int * encodeNegativePositiveNumber(int * samples, unsigned short channels);

    int * encodeTemporalRedundancy(int *p_sample, Predictor *preds,  unsigned short channels);

    int * encodeLossy(int *p_sample, int desloc,  unsigned short channels);

    int *encodeChannelRedundancy(int *p_sample);

    void encode(int mode);

    void GolombToFile(int mode);
};


#endif //PROJECT2_WAVencoder_H
