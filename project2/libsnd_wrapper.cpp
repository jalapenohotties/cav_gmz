#include "libsnd_wrapper.h"


libsnd_wrapper::libsnd_wrapper(const char *path_in) {
    name = path_in;
}

short *libsnd_wrapper::getSample() {
    checkAlreadyRead();
    //short *p_sample = NULL;
    short *p_sample = new short[soundInfoIn.channels];// tem de se pre alocar mem. lib snd nao trata disso -.-
    if (sf_readf_short(p_soundFile, p_sample, nSamples) == 0) {
        fprintf(stderr, "Error: Reached end of file\n");
        sf_close(p_soundFile);
        return NULL;
    }
    return p_sample;
}

void libsnd_wrapper::setSample(const short *p_sample) {
    checkAlreadyWrote();
    if (sf_writef_short(p_soundFile, p_sample, nSamples) != 1) {
        fprintf(stderr, "Error writing frames to the output:\n");
        sf_close(p_soundFile);
        return;
    }
}

uint libsnd_wrapper::getChannels() {
    checkAlreadyRead();
    return (uint) soundInfoIn.channels;
}

uint libsnd_wrapper::getFrames() {
    checkAlreadyRead();
    return (uint) (int) soundInfoIn.frames;

}

uint libsnd_wrapper::getSampleRate() {
    checkAlreadyRead();
    return (uint) soundInfoIn.samplerate;
}

uint libsnd_wrapper::getFormat() {
    checkAlreadyRead();
    return (uint) soundInfoIn.format;
}


void libsnd_wrapper::checkAlreadyWrote() {
    if (!alreadyWrote) {
        p_soundFile = sf_open(name, SFM_WRITE, &soundInfoIn);
        if (p_soundFile == NULL)
            std::cerr << "libsnd_wrapper soundFileOut is null for write" << endl;
        alreadyWrote = true;
    }
}

void libsnd_wrapper::checkAlreadyRead() {
    //nunca foi lido, nunca foi escrito nao tem valores default
    if (!alreadyRead && !alreadyWrote &&
        (soundInfoIn.channels == 0 && soundInfoIn.format == 0 && soundInfoIn.frames == 0 &&
          soundInfoIn.samplerate == 0)) {
        p_soundFile = sf_open(name, SFM_READ, &soundInfoIn);
        if (p_soundFile == NULL)
            std::cerr << "libsnd_wrapper soundFileIn is null for read:" << name << endl;
        alreadyRead = true;
    }
}

void libsnd_wrapper::setSampleRate(int samplerate) {
    soundInfoIn.samplerate = samplerate;
}

void libsnd_wrapper::setChannels(int channels) {
    soundInfoIn.channels = channels;
}

void libsnd_wrapper::setFrames(int frames) {
    soundInfoIn.frames = frames;
}

void libsnd_wrapper::setFormat(int format) {
    soundInfoIn.format = format;
}

void libsnd_wrapper::setSampleRateBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset) {
    setSampleRate((int) bitset.to_ulong());
}

void libsnd_wrapper::setFramesBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset) {
    setFrames((int) bitset.to_ulong());
}

void libsnd_wrapper::setFormatBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset) {
    setFormat((int) bitset.to_ulong());
}

void libsnd_wrapper::setChannelsBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset) {
    setChannels((int) bitset.to_ulong());
}

void libsnd_wrapper::closeFile() {
    sf_close(p_soundFile);
}