#include "CAVdecoder.h"


CAVdecoder::CAVdecoder(const char *path_CAV_in, const char *path_WAV_out) :
        path_CAV_in(path_CAV_in), path_WAV_out(path_WAV_out) {

    decode();

}

void CAVdecoder::init(int numFrames) {

    p_differences = new vector<short>[wavFileOut->getChannels()];
    p_predict = new short[wavFileOut->getChannels()];

    // initialize predictors buffers and others
    for (int i = 0; i < wavFileOut->getChannels(); i++) {
        p_differences[i] = vector<short>((unsigned long) numFrames);
    }

}

/*
 * Operation mode:
 * 1 - temporal redundancy
 * 2 - channel redundancy
 * 3 - lossy
 * */
void CAVdecoder::decode() {

    //libsnd_wrapper *wavFileOut = new libsnd_wrapper("./cenas.wav");
    //BitStream *bs_out_g = new BitStream("./teste_golomb.cav");

    wavFileOut = new libsnd_wrapper(path_WAV_out);
    BitStream *bs_out_g = new BitStream(path_CAV_in);

    wavFileOut->setSampleRateBS(bs_out_g->get_nBit(32));
    wavFileOut->setChannelsBS(bs_out_g->get_nBit(8));
    wavFileOut->setFormatBS(bs_out_g->get_nBit(32));
    int numFrames = (int) bs_out_g->get_nBit(32).to_ulong();
    int mode = (int) bs_out_g->get_nBit(32).to_ulong();
    int lossBits;
    if (mode == 3)
        lossBits = (int) bs_out_g->get_nBit(32).to_ulong();;


    init(numFrames);

    //get m for all channels
    unsigned short channels = wavFileOut->getChannels();
    int *p_m = new int[channels];
    for (unsigned short c = 0; c < channels; c++) {
        p_m[c] = (int) bs_out_g->get_nBit(32).to_ulong();
        p_predict[c] = 0;
    }

    Golomb t2 = Golomb(bs_out_g);
    Predictor *preds = new Predictor(channels);
    unsigned int *decodedVal = new unsigned int[channels];
    short *out;

    unsigned short c;

    //decode with golomb
    for (sf_count_t i = 0; i < numFrames; i++) {
        for (int short c = 0; c < channels; c++) {
            t2.setTunable(p_m[c]);
            decodedVal[c] =  t2.decode();
        }

        switch (mode) {
            case 1:
                out = decodeNegativePositiveNumber(decodedVal, channels);
                out = decodeTemporalRedundancy(out, preds, channels);
                break;
            case 2:
                out = decodeNegativePositiveNumber(decodedVal, channels);
                out = decodeTemporalRedundancy(out, preds, channels);
                out = decodeChannelRedundancy(out, channels);
                break;
            case 3:
                out = decodeNegativePositiveNumber(decodedVal, channels);
                out = decodeTemporalRedundancy(out, preds, channels);
                break;
        }

        for (c = 0; c < wavFileOut->getChannels(); c++) {
            p_differences[c][i] = out[c];
        }
    }


    for (sf_count_t i = 0; i < numFrames; i++) {
        short *p_sample = new short[wavFileOut->getChannels()];
        for (unsigned int c = 0; c < wavFileOut->getChannels(); c++) {
            p_sample[c] = p_differences[c][i];
        }

        wavFileOut->setSample(p_sample);
    }
    wavFileOut->closeFile();
}

short *CAVdecoder::decodeTemporalRedundancy(short *decodedVal, Predictor *preds, unsigned short channels) {

    short *out = new short[channels];
    for (unsigned short c = 0; c < channels; c++) {
        out[c] = decodedVal[c] + p_predict[c];
        p_predict[c] = preds->predictor_3n(out[c], c);
    }
    return out;
}

short *CAVdecoder::decodeChannelRedundancy(short *p_sample, unsigned short channels) {
    short *out = new short[channels];
    /*
     A ideia é, no descodificador, ler primeiro o Y.
     Se for par, então L+R é também par e temos L+R = 2X.
     Se Y for ímpar, então temos que fazer L+R = 2X+1.
     A partir destas relações obtêm-se o L e o R.
     */

    out[0] = (p_sample[0] + p_sample[1])/2;
    out[1] = (p_sample[0] - p_sample[1])/2;
    return out;
}

short *CAVdecoder::decodeLossy(short *p_sample, int desloc, unsigned short channels) {

    short *out = new short[channels];
    for (unsigned short c = 0; c < channels; c++) {
        out[c] = p_sample[c] << desloc;
    }

    return out;
}

short *CAVdecoder::decodeNegativePositiveNumber(unsigned int *value, unsigned short channels) {

    short *decodedVal = new short[channels];

    // FIXME: why the f--- do i need that int cast?
    for (unsigned short c = 0; c < channels; c++)
        decodedVal[c] = (value[c] % 2 == 0) ? value[c] / 2 : (int)(1 + value[c]) / -2;

    return decodedVal;
}
