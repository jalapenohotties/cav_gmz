#include "Predictor.hpp"


Predictor::Predictor(int number_channels) {

    predictor_buffer = new boost::circular_buffer<short>[number_channels]();

    for (int j = 0; j < number_channels; j++) {
        predictor_buffer[j].set_capacity(ORDER);

        for (int i = 0; i < ORDER; i++)
            predictor_buffer[j].push_back(0);

    }
}

Predictor::~Predictor() {
    delete predictor_buffer;
}

short Predictor::predictor_3n(short sample, unsigned short channel) {
    predictor_buffer[channel].push_back(sample);
    return 3 * predictor_buffer[channel][2] - 3 * predictor_buffer[channel][1] + predictor_buffer[channel][0];
}