#include <sndfile.h>
#include <iostream>
#include <boost/dynamic_bitset.hpp>

using namespace std;

#ifndef PROJECT2_libsnd_wrapper_H
#define PROJECT2_libsnd_wrapper_H


class libsnd_wrapper {
private:
    bool alreadyWrote;
    bool alreadyRead;
    const char *name;


    sf_count_t nSamples = 1;
    SNDFILE *p_soundFile;
    SF_INFO soundInfoIn;

public:

    uint getChannels();

    uint getFrames();

    short * getSample();

    uint getSampleRate();

    uint getFormat();

    void checkAlreadyWrote();

    void setSampleRate(int i);

    void setChannels(int i);

    void setFormat(int i);

    void setFrames(int i);

    void setSampleRateBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset);

    void setChannelsBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset);

    void setFormatBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset);

    void setFramesBS(boost::dynamic_bitset<unsigned long, allocator<unsigned long>> bitset);

    libsnd_wrapper(const char *path_in);

    void closeFile();

    void setSample(const short *p_sample);

    void checkAlreadyRead();
};


#endif //PROJECT2_libsnd_wrapper_H
