#include "WAVencoder.h"
#include "Golomb.hpp"

WAVencoder::WAVencoder(const char *path_in,
                       const char *path_out,
                       bool channel_redundancy) : path_in(path_in),
                                                  path_out(path_out),
                                                  channel_redundancy(channel_redundancy) {


    init();

    if (channel_redundancy)
        encode(2);  // encode lossy using channel redundancy
    else
        encode(1);  // encode lossy using temporal redundancy

}

WAVencoder::WAVencoder(const char *path_in,
                       const char *path_out,
                       int redundancy_bits) : path_in(path_in),
                                              path_out(path_out),
                                              redundancy_bits(redundancy_bits) {

    init();
    encode(3);      // encode lossy discarding bits
}

void WAVencoder::init() {

    if (path_in == NULL || path_out == NULL) {
        cerr << "Paths can't be null";
        throw;
    }

    // GolombToFile lossless with channel redundancy or not
    wavFileIn = new libsnd_wrapper(path_in);
    assert (wavFileIn != NULL);

    p_histogram_before = new map<short, unsigned int>[wavFileIn->getChannels()];
    p_histogram_after = new map<short, double>[wavFileIn->getChannels()];
    p_differences = new vector<unsigned int>[wavFileIn->getChannels()];
    p_predict = new short[wavFileIn->getChannels()];

    // initialize predictors buffers and others
    for (int i = 0; i < wavFileIn->getChannels(); i++) {
        p_histogram_before[i] = {};
        p_histogram_after[i] = {};
        p_differences[i] = vector<unsigned int>((unsigned long) wavFileIn->getFrames());
    }

}

/*
 * Operation mode:
 * 1 - temporal redundancy
 * 2 - channel redundancy
 * 3 - lossy
 * */
void WAVencoder::encode(int mode) {

    unsigned short channels = wavFileIn->getChannels();
    sf_count_t frames = wavFileIn->getFrames();

    int *out = new int[channels];
    short *p_sample;
    Predictor *preds = new Predictor(channels);
    unsigned short c;

    if (mode == 2 && channels > 2) {
        cerr << "Channel redundancy works for two channels only" << endl;
        throw;
    }

    for (sf_count_t i = 0; i < frames; i++) {

        if ((p_sample = wavFileIn->getSample()) == NULL)
            break;

        for(c = 0; c < channels; c++)
            out[c] = (int)p_sample[c];

        switch (mode) {
            case 1:
                out = encodeTemporalRedundancy(out, preds, channels);
                out = encodeNegativePositiveNumber(out, channels);
                break;
            case 2:
                out = encodeChannelRedundancy(out);
                out = encodeTemporalRedundancy(out, preds, channels);
                out = encodeNegativePositiveNumber(out, channels);
                break;
            case 3:
                out = encodeLossy(out, redundancy_bits, channels);
                out = encodeTemporalRedundancy(out, preds, channels);
                out = encodeNegativePositiveNumber(out, channels);
                break;
        }

        for (c = 0; c < channels; c++) {
            p_differences[c][i] = out[c];
            p_histogram_before[c][p_sample[c]]++;
            p_histogram_after[c][p_differences[c][i]]++;
        }
    }

    wavFileIn->closeFile();

    GolombToFile(mode); // write result to file
}

int *WAVencoder::calcOptimalM() {
    // for each channel...
    int *p_m = new int[wavFileIn->getChannels()];
    for (unsigned short c = 0; c < wavFileIn->getChannels(); c++) {
        // find optimum M by calling gnuplot fitting function for p of f(x) = (1-p) * p**x
        ofstream tmp_file;
        tmp_file.open(string(TMP_FILE_PATH) + to_string(c));

        for (auto it: p_histogram_after[c])
            tmp_file << it.first << "\t" << setprecision(PRECISION) << it.second / wavFileIn->getFrames() << "\n";

        tmp_file.close();

        FILE *pipe = popen(GNUPLOT_POPEN, "w");
        if (!pipe)
            throw runtime_error("popen() failed!");
        try {
            fprintf(pipe, string("set print '" + filepath(c) + ".m'\n").c_str());
            fprintf(pipe, "set fit quiet\n");
            fprintf(pipe, "f(x) = (1-p) * p**x\n");
            fprintf(pipe, string("fit [x=0:2500] f(x) '" + filepath(c) + "' using 1:2 via p\n").c_str());
            fprintf(pipe, "print ceil(-(1/(log(p)/log(2))))\n");
            fflush(pipe);
        } catch (...) {
            pclose(pipe);
            throw;
        }
        pclose(pipe);
        // once optimum M is determined, read result and GolombToFile p_differences using Golomb
        fstream gnuplot_res(filepath(c) + ".m", std::ios_base::in);
        gnuplot_res >> p_m[c]; // read optimum golomb for channel
        cout << "Golomb optimo final canal:" << c << ":" << p_m[c] << endl;
        gnuplot_res.close();
    }

    return p_m;
}

void WAVencoder::GolombToFile(int mode) {

    int *p_m = calcOptimalM(); //remover daqui para optimizar o pre-calc
    bs = new BitStream(path_out);

    bs->write_value(wavFileIn->getSampleRate(), 32);
    bs->write_value(wavFileIn->getChannels(), 8);
    bs->write_value(wavFileIn->getFormat(), 32);
    bs->write_value(wavFileIn->getFrames(), 32);
    bs->write_value(mode, 32);
    if (mode == 3)
        bs->write_value(redundancy_bits, 32);

    for (int i = 0; i < wavFileIn->getChannels(); i++) {
        bs->write_value(p_m[i], 32);
    }

    Golomb g = Golomb(bs);

    unsigned short channels = wavFileIn->getChannels();
    sf_count_t frames = wavFileIn->getFrames();
    unsigned short c;

    for (sf_count_t i = 0; i < frames; i++) {
        for (c = 0; c < channels; c++) {
            g.setTunable(p_m[c]);
            // send all channel residuals to golomb to write to file
            //cout << p_differences[c][i] << endl;
            g.encode_toBitStream(p_differences[c][i]);
        }
    }

    bs->flushAndClose();
}

int *WAVencoder::encodeTemporalRedundancy(int *p_sample, Predictor *preds, unsigned short channels) {
    int difference = 0;
    int *out = new int[channels];
    for (unsigned short c = 0; c < channels; c++) {
        // calculate the difference between predictor and actual value
        difference = p_sample[c] - p_predict[c];
        // get the next predictor
        p_predict[c] = preds->predictor_3n(p_sample[c], c);
        // GolombToFile negative and positive values to only positive values
        out[c] = difference;
    }
    return out;
}

string WAVencoder::filepath(int c) {
    return string(string(TMP_FILE_PATH) + to_string(c));
}

/* GolombToFile negative numbers according to: * if x < 0: 2|x| + 1 * else 2|x|  */
int *WAVencoder::encodeNegativePositiveNumber(int *samples, unsigned short channels) {
    int *out = new int[channels];

    for (unsigned short c = 0; c < channels; c++)
        out[c] = samples[c] < 0 ? (-2 * samples[c]) - 1 : samples[c] * 2;

    return out;
}


int *WAVencoder::encodeLossy(int *p_sample, int desloc, unsigned short channels) {
    int *out = new int[channels];

    for (int c = 0; c < channels; c++)
        out[c] = p_sample[c] >> desloc;

    return out;
}

int *WAVencoder::encodeChannelRedundancy(int *p_sample) {
    int *out = new int[2];
    out[0] = p_sample[0] + p_sample[1];
    out[1] = p_sample[0] - p_sample[1];
    return out;
}

void WAVencoder::printStats() {

    sf_count_t frames = wavFileIn->getFrames();
    double sum = 0; // -sum(p *log(p))
    double p;

    // initial entropy
    for (auto it : *p_histogram_before) {
        p = (double) it.second / frames;
        sum += p * log2(p);
    }
    cout << "Initial entropy: " << setprecision(STATS_PRECISION) << -sum << endl;

    // final entropy
    sum = 0;
    for (auto it : *p_histogram_after) {
        p = it.second / frames;
        sum += p * log2(p);
    }
    cout << "Final entropy: " << setprecision(STATS_PRECISION) << -sum << endl;

    // compression ratio
    ifstream original_file(path_in, ifstream::binary | fstream::ate);
    unsigned int original_size = original_file.tellg();
    original_file.close();

    // FIXME: why is path_out variable destroyed?
    cout << "in:" << path_in << " out:" << path_out << endl;
    ifstream encoded_file(string(path_in) + string(".cav"), ifstream::binary | fstream::ate);
    unsigned int final_size = encoded_file.tellg();
    encoded_file.close();

    double ratio = (double) final_size / original_size;
    cout << "Compression ratio of " << setprecision(STATS_PRECISION) << ratio << "x" << endl;
    cout << "Compression factor of " << setprecision(STATS_PRECISION) << (1 - ratio) * 100 << endl;

    // all console stats printed, show graphs for just one channel.
    // just to avoid displaying channels * 2 graphs
    cout << "Generating graphs for channel 0..." << endl;

    // calculate data for cav file
    ofstream tmp_file;
    tmp_file.open(string(TMP_FILE_PATH) + string("wav"));

    for (auto it: p_histogram_before[0])
        tmp_file << it.first << "\t" << setprecision(PRECISION) << (double) it.second / wavFileIn->getFrames() << "\n";

    tmp_file.close();

    FILE *pipe = popen(GNUPLOT_POPEN, "w");
    if (!pipe)
        throw runtime_error("popen() failed!");
    try {
        fprintf(pipe, "set fit quiet\n");
        fprintf(pipe, "set output 'histogram_cav.png'\n");
        fprintf(pipe, "set terminal png size 800,600 enhanced font \"Helvetica,8\"\n");
        fprintf(pipe, "f(x) = (1-p) * p**x\n");
        fprintf(pipe, "fit [x=0:2500] f(x) '/tmp/gnuplot.tmp.0' using 1:2 via p\n");
        fprintf(pipe, "plot '/tmp/gnuplot.tmp.0' using 1:2, (1-p) * p**x\n");
        fprintf(pipe, "set output 'histogram_wav.png'\n");
        fprintf(pipe, "fit [x=0:2500] f(x) '/tmp/gnuplot.tmp.wav using 1:2 via p\n");
        fprintf(pipe, "plot '/tmp/gnuplot.tmp.wav' using 1:2\n");
        fflush(pipe);
    } catch (...) {
        pclose(pipe);
        throw;
    }
    pclose(pipe);

}
